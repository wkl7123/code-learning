#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "allvars.h"
#include "proto.h"
#include "define.h"

void write_file()
{
  int i;
  char file_name[500];
  FILE *fp;

#ifndef NOFILE_OUTPUT  
  sprintf(file_name, "%s%s%.3f%s", OUTPUT_PATH, "g_power_z_", g_head.Redshift, ".txt");
  if((fp = fopen(file_name, "w+")) == NULL)    
    {
	 printf("Fail to open output file %s\n", file_name);
	 exit(0);
	}
#endif
    
  for(i = 0; i < BIN_NUMBER; i++)
    {			
      printf("%.6e	%.6e	%d\n", pow(10, p_bin[i].log_k), p_bin[i].p * pow(boxsize, 3) / pow(total_grid_num, 2), p_bin[i].n);
#ifndef NOFILE_OUTPUT      
      fprintf(fp, "%.6e	%.6e	%d\n", pow(10, p_bin[i].log_k), p_bin[i].p * pow(boxsize, 3) / pow(total_grid_num, 2), p_bin[i].n);	
#endif      
    }
#ifndef NOFILE_OUTPUT
  fclose(fp);	
#endif
}    /* end write file */	
