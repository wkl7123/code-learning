#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "allvars.h"

 GRID ***pg;
 double *fft_data;
 POW_BIN *p_bin;

 int ng;                 /* grid number in each dimension */
 int gadget_file_num;
 double boxsize;
 double grid_dis;
 double total_mass;
 long int total_part_num;
 double total_grid_num;
 
 GADGET_HEAD g_head;
