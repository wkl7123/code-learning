#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "allvars.h"
#include "proto.h"
#include "define.h"


void alloc_3d_array()
{
 int i,j;

  pg = (GRID ***) malloc(ng * sizeof(GRID **));

  for(i=0; i<ng ; i++)
    pg[i] = (GRID **) malloc(ng * sizeof(GRID *));
   
  for(i=0; i<ng ; i++)
    for(j=0; j<ng ; j++)
       pg[i][j] = (GRID *) malloc(ng * sizeof(GRID));
}                              /* end create_3d_array */

void free_3d_array(GRID *** array)
{
 int i,j;
  
 for(i=0; i<ng; i++)
   for(j=0; j<ng ; j++)
      free(array[i][j]);

 for(i=0; i<ng; i++)
   free(array[i]);

 free(array);
}                               /* end free_3d_array */
  

void alloc_fft_data()
{
  if(( fft_data = (double *) malloc(2 * ng * sizeof(double)) ) == NULL)
    warn_and_end("Fail to allocate memory for the FFT data"); 
}                               /* end alloc_fft_data */

void alloc_power_bin_array()
{
  if(( p_bin = (POW_BIN *) malloc(BIN_NUMBER * sizeof(POW_BIN)) ) == NULL)
    warn_and_end("Fail to allocate memory for the POWER_BIN data"); 		
}	


void free_all()
{  
  free_3d_array(pg);
  
  free(fft_data);
  
  free(p_bin);
		
}   /* end free_all */	
