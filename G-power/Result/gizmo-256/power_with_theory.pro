;file="./20.txt"
Pro power_with_theory,file
line=file_lines(file)
;if n_params() eq 1 then init_line=0
openr,1,file
data=fltarr(3,line)
readf,1,data
init_line=0
;FOR i=line-1,0,-1 do begin
;	if data[2,i] eq 0 then begin
;		init_line=i+1
;		break
;	endif
;ENDFOR
;print,'init_line=',init_line
;help,data
close,1
set_plot,"PS"
device,filename="/home/allen/PS/temp/power_spectrum.eps"
x=alog10(data[0,init_line:line-1])
y=alog10(data[1,init_line:line-1])
plot,x,y,psym=2,xtitle='log K [/Mpc]',ytitle='log P(k)',charsize=1.5
openr,1,'camb_71437748_matterpower_z0.dat'
dt=dblarr(2,505)
readf,1,dt
dt=alog10(dt)
for i=0,504 do begin
	dt[1,i]+=alog10((0.9/0.82739317)^2)
endfor
oplot,dt[0,*],dt[1,*]
device,/close_file
END

