#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "allvars.h"
#include "proto.h"
#include "define.h"


int main()
{
  init_all();
  state("Initilzation done ...");
  
  load_gedget_part_to_array();
  state("Loading Gadget done ...");
  
  grid_transfer();
  state("FFT done ...");
  
  collect_power_bin_data();
  state("Power done ...");
  
  write_file();
  state("Output done ...");
  
  free_all();

  return 1;
}   /* end main */
