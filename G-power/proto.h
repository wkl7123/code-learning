#ifndef ALLVAR_H
 #include "allvars.h"
#endif

void assign_part(float *pos, float mass);

void grid_transfer();
void fft_3d(int flag, GRID ***f);
void flip_data();
void copy_to_fft_array(int i, int j, int k, GRID ***f);
void copy_from_fft_array(int i, int j, int k, GRID *** f);

void init_all();

void detect_and_link_gadget_file();
void load_gedget_part_to_array();
void wrap_pos(float *pos);
void check_gadget_file(char *file_name);

void alloc_3d_array();
void free_3d_array(GRID *** array);
void alloc_fft_data();
void alloc_power_bin_array();
void free_all();

void write_file();

void collect_power_bin_data();
void init_power_bin(double k_min, double log_k_dis);

void warn_and_end(char *s);
void state(char *s);
